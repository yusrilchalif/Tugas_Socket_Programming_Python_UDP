import socket   #library untuk socket
import sys      #library untuk runtime ke environtment
 
# membuat datagram udp socket
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print 'Gagal membuat socket'
    sys.exit()
 
host = 'localhost';
port = 8888;
 
while(1) :
    msg = raw_input('Masukkan Pesan : ')
     
    try :
        #Atur karakter untuk chat
        s.sendto(msg, (host, port))
         
        # menerima data dari client (data, addr)
        d = s.recvfrom(1024)
        reply = d[0]
        addr = d[1]
         
        print 'Server membalas : ' + reply
     
    except socket.error, msg:
        print 'Error Code : ' + str(msg[0]) + ' Pesan' + msg[1]
        sys.exit()
