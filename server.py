import socket
import sys
 
HOST = ''   
PORT = 8888
 
# Datagram (udp) socket
try :
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print 'Socket telah dibuat'
except socket.error, msg :
    print 'Gagal membuat socket. Error Code : ' + str(msg[0]) + ' Pesan ' + msg[1]
    sys.exit()
 
 
# Bind socket ke local host
try:
    s.bind((HOST, PORT))
except socket.error , msg:
    print 'Bind gagal dibuat. Error Code : ' + str(msg[0]) + ' Pesan ' + msg[1]
    sys.exit()
     
print 'Socket bind telah selesai'
 
#server chat dibuat
while 1:
    # menerima data dari client (ip , port)
    d = s.recvfrom(1024)
    data = d[0]
    addr = d[1]
     
    if not data: 
        break
     
    reply = 'Pesan Terkirim ke...' + addr[0]
     
    s.sendto(reply , addr)
    print 'Pesan[' + addr[0] + ':' + str(addr[1]) + '] - ' + data.strip()
     
s.close()
